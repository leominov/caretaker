package main

import (
	"fmt"

	"github.com/mitchellh/colorstring"
)

type report struct {
	repos      []*repository
	hasUpdates bool
	hasErrors  bool
}

func (r *report) Print() {
	if !r.hasUpdates && !r.hasErrors {
		fmt.Println("Everything is up to date.")
		return
	}
	if r.hasUpdates {
		fmt.Println("Here is some updates for you, enjoy:")
		fmt.Println()
		printsRepos(r.repos)
	}
	if r.hasErrors {
		fmt.Println(colorstring.Color("Errors:"))
		fmt.Println()
		printsErrors(r.repos)
	}
}
