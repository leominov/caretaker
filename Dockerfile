FROM golang:1.14-alpine3.12 as builder
WORKDIR /go/src/github.com/leominov/caretaker
COPY . .
RUN apk add --no-cache make && make build

FROM alpine:3.12
COPY --from=builder /go/src/github.com/leominov/caretaker/caretaker /usr/local/bin/caretaker
