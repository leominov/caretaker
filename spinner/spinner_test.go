package spinner

import (
	"testing"
	"time"
)

func TestNew(t *testing.T) {
	s := New(time.Second)
	if s == nil {
		t.Error("New() == nil")
	}
}

func TestStart(t *testing.T) {
	s := New(time.Second)
	s.isTerminal = true
	s.Start()
	s.Stop()
	s.isTerminal = false
	s.Start()
	s.Stop()
}
