package spinner

import (
	"os"
	"time"

	"github.com/briandowns/spinner"
	"golang.org/x/crypto/ssh/terminal"
)

type Spinner struct {
	spinner    *spinner.Spinner
	isTerminal bool
}

func New(d time.Duration) *Spinner {
	sp := &Spinner{}
	sp.spinner = spinner.New(spinner.CharSets[11], d)
	sp.spinner.Writer = os.Stderr

	if terminal.IsTerminal(int(os.Stdout.Fd())) {
		sp.isTerminal = true
	}

	return sp
}

func (s *Spinner) Start() {
	if !s.isTerminal {
		return
	}
	s.spinner.Start()
}

func (s *Spinner) Stop() {
	if !s.isTerminal {
		return
	}
	s.spinner.Stop()
}

func (s *Spinner) SetPrefix(prefix string) {
	s.spinner.Prefix = prefix
}
