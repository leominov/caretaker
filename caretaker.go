package main

import (
	"errors"
	"fmt"
	"time"

	"github.com/coreos/go-semver/semver"

	"github.com/leominov/caretaker/discoverer"
	_ "github.com/leominov/caretaker/discoverer/ansiblerequirements"
	_ "github.com/leominov/caretaker/discoverer/environ"
	"github.com/leominov/caretaker/provider"
	"github.com/leominov/caretaker/spinner"
	"github.com/leominov/changelog"
)

type Caretaker struct {
	WorkingDir string
	Config     *Config
}

func (c *Caretaker) IsExcluded(dep *discoverer.Dependency) bool {
	if len(c.Config.Exclude) == 0 {
		return false
	}
	for _, exp := range c.Config.Exclude {
		if exp.MatchDependency(dep) {
			return true
		}
	}
	return false
}

func (c *Caretaker) processDependencies(deps []*discoverer.Dependency) *report {
	r := &report{}
	s := spinner.New(100 * time.Millisecond)
	s.SetPrefix(fmt.Sprintf("Processing %d dependencies...", len(deps)))
	s.Start()
	for i, dep := range deps {
		s.SetPrefix(fmt.Sprintf("Processed %d/%d repositories...", i+1, len(deps)))
		if srv, err := c.providerByDependency(dep); err == nil {
			dep.Provider = srv.GetName()
		}
		if c.IsExcluded(dep) {
			continue
		}
		repo := c.processDependency(dep)
		if len(repo.errors) > 0 {
			r.hasErrors = true
		}
		if repo.hasUpdate {
			r.hasUpdates = true
		}
		r.repos = append(r.repos, repo)
	}
	s.Stop()
	return r
}

func (c *Caretaker) Run() int {
	deps, err := discoverer.Discover(c.WorkingDir)
	if err != nil {
		printsError(err)
		return 1
	}
	printsHeader()
	report := c.processDependencies(deps)
	report.Print()
	if report.hasErrors {
		return 1
	}
	return 0
}

func (c *Caretaker) processDependency(dep *discoverer.Dependency) *repository {
	repo := &repository{
		dep: dep,
	}
	ver, err := semver.NewVersion(SanitizedVersion(dep.Version))
	if err != nil {
		repo.addError(fmt.Sprintf("Failed to parse %q version", dep.Version))
		return repo
	}
	repo.version = &Semver{
		Version: ver,
		Raw:     dep.Version,
	}
	chlog, err := c.getChangelog(dep, "master")
	if err == nil {
		repo.changelog = chlog
		repo.release = chlog.GetRelease(repo.version.Version.String())
		if repo.release == nil {
			repo.addError(fmt.Sprintf("Failed to find %s in changelog", repo.version.Raw))
		} else {
			repo.yanked = repo.release.Yanked
			if repo.yanked {
				repo.addError("Current release was yanked")
			}
		}
	}
	tags, err := c.getTags(dep)
	if err != nil {
		repo.addError(fmt.Sprintf("Failed to get tags: %v", err))
		return repo
	}
	repo.tags = tags
	repo.major = FindNewestMajorTag(repo.tags, repo.version)
	repo.minor = FindNewestMinorTag(repo.tags, repo.version)
	repo.patch = FindNewestPatchTag(repo.tags, repo.version)
	if len(repo.major) > 0 {
		if c.Config.Fail.On.Major {
			repo.addError(fmt.Sprintf("Major update are available: %s", repo.major))
		}
		repo.hasUpdate = true
	}
	if len(repo.minor) > 0 {
		if c.Config.Fail.On.Minor {
			repo.addError(fmt.Sprintf("Minor update are available: %s", repo.minor))
		}
		repo.hasUpdate = true
	}
	if len(repo.patch) > 0 {
		if c.Config.Fail.On.Patch {
			repo.addError(fmt.Sprintf("Patch update are available: %s", repo.patch))
		}
		repo.hasUpdate = true
	}
	return repo
}

func (c *Caretaker) providerByDependency(dep *discoverer.Dependency) (provider.Provider, error) {
	srv, ok := provider.Providers[dep.Provider]
	if ok {
		return srv, nil
	}
	for _, prov := range c.Config.Providers {
		if prov.Hostname != dep.URL.Hostname() {
			continue
		}
		srv, ok := provider.Providers[prov.Type]
		if ok {
			return srv, nil
		}
	}
	return nil, errors.New("unknown provider")
}

func (c *Caretaker) getTags(dep *discoverer.Dependency) ([]*Semver, error) {
	srv, err := c.providerByDependency(dep)
	if err != nil {
		return nil, err
	}
	tags, err := srv.GetTags(dep.Repository)
	if err != nil {
		return nil, err
	}
	var semverTags []*Semver
	for _, tag := range tags {
		v, err := semver.NewVersion(SanitizedVersion(tag))
		if err != nil {
			continue
		}
		s := &Semver{
			Version: v,
			Raw:     tag,
		}
		semverTags = append(semverTags, s)
	}
	return semverTags, nil
}

func (c *Caretaker) getChangelog(dep *discoverer.Dependency, ref string) (*changelog.Changelog, error) {
	srv, err := c.providerByDependency(dep)
	if err != nil {
		return nil, err
	}
	body, err := srv.GetChangelog(dep.Repository, ref)
	if err != nil {
		return nil, err
	}
	return changelog.Parse(body)
}
