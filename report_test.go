package main

import "testing"

func TestPrint(t *testing.T) {
	r := &report{}
	r.Print()
	r = &report{
		hasUpdates: true,
	}
	r.Print()
	r = &report{
		hasErrors: true,
	}
	r.Print()
}
