package main

import (
	"testing"

	"github.com/coreos/go-semver/semver"
)

func TestSanitizedVersion(t *testing.T) {
	tests := map[string]string{
		"v.1.0.0": "1.0.0",
		"v1.0.0":  "1.0.0",
		"1.0.0.":  "1.0.0.",
	}
	for in, out := range tests {
		result := SanitizedVersion(in)
		if result != out {
			t.Errorf("Must be %s, but got %s", out, result)
		}
	}
}

func TestFindNewestMajorTag(t *testing.T) {
	tests := []struct {
		in   string
		tags []string
		out  string
	}{
		{
			in: "1.0.0",
			tags: []string{
				"1.0.0",
				"2.0.0",
				"2.2.2",
				"3.3.0",
				"3.0.0",
			},
			out: "3.3.0",
		},
		{
			in: "1.0.0",
			tags: []string{
				"1.0.0",
				"1.1.0",
				"1.2.2",
				"1.3.0",
			},
			out: "",
		},
	}
	for _, test := range tests {
		v, err := semver.NewVersion(test.in)
		if err != nil {
			t.Error(err)
		}
		inS := &Semver{
			Version: v,
			Raw:     test.in,
		}
		var tagsS []*Semver
		for _, tag := range test.tags {
			v, err := semver.NewVersion(tag)
			if err != nil {
				t.Error(err)
			}
			tagsS = append(tagsS, &Semver{
				Version: v,
				Raw:     tag,
			})
		}
		out := FindNewestMajorTag(tagsS, inS)
		if out != test.out {
			t.Errorf("Must be %s, but got %s", test.out, out)
		}
	}
}

func TestFindNewestMinorTag(t *testing.T) {
	tests := []struct {
		in   string
		tags []string
		out  string
	}{
		{
			in: "1.0.0",
			tags: []string{
				"1.0.0",
				"2.0.0",
				"1.2.2",
				"3.3.0",
				"3.0.0",
				"1.5.5",
			},
			out: "1.5.5",
		},
		{
			in: "1.0.0",
			tags: []string{
				"1.0.0",
				"2.1.0",
				"3.2.2",
				"4.3.0",
			},
			out: "",
		},
	}
	for _, test := range tests {
		v, err := semver.NewVersion(test.in)
		if err != nil {
			t.Error(err)
		}
		inS := &Semver{
			Version: v,
			Raw:     test.in,
		}
		var tagsS []*Semver
		for _, tag := range test.tags {
			v, err := semver.NewVersion(tag)
			if err != nil {
				t.Error(err)
			}
			tagsS = append(tagsS, &Semver{
				Version: v,
				Raw:     tag,
			})
		}
		out := FindNewestMinorTag(tagsS, inS)
		if out != test.out {
			t.Errorf("Must be %s, but got %s", test.out, out)
		}
	}
}

func TestFindNewestPatchTag(t *testing.T) {
	tests := []struct {
		in   string
		tags []string
		out  string
	}{
		{
			in: "1.0.0",
			tags: []string{
				"1.0.0",
				"1.0.1",
				"1.1.0",
				"1.2.2",
				"1.3.0",
				"1.0.3",
			},
			out: "1.0.3",
		},
		{
			in: "1.0.0",
			tags: []string{
				"1.0.0",
				"2.0.0",
				"1.2.2",
				"3.3.0",
				"3.0.0",
				"1.5.5",
			},
			out: "",
		},
	}
	for _, test := range tests {
		v, err := semver.NewVersion(test.in)
		if err != nil {
			t.Error(err)
		}
		inS := &Semver{
			Version: v,
			Raw:     test.in,
		}
		var tagsS []*Semver
		for _, tag := range test.tags {
			v, err := semver.NewVersion(tag)
			if err != nil {
				t.Error(err)
			}
			tagsS = append(tagsS, &Semver{
				Version: v,
				Raw:     tag,
			})
		}
		out := FindNewestPatchTag(tagsS, inS)
		if out != test.out {
			t.Errorf("Must be %s, but got %s", test.out, out)
		}
	}
}
