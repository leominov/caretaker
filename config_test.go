package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadFromFile(t *testing.T) {
	config, err := LoadFromFile("not-found")
	assert.NoError(t, err)
	assert.NotNil(t, config)
	_, err = LoadFromFile("./test_data")
	assert.Error(t, err)
	_, err = LoadFromFile("./test_data/config_invalid_syntax.yaml")
	assert.Error(t, err)
	_, err = LoadFromFile("./test_data/config_valid.yaml")
	assert.NoError(t, err)
	assert.NotNil(t, config)
}
