package main

import (
	"fmt"
	"regexp"
	"sort"
	"strings"

	humanize "github.com/dustin/go-humanize"
	"github.com/mitchellh/colorstring"
	"github.com/ryanuber/columnize"
)

const (
	header = `
	 _______  _______  _______  _______ _________ _______  _        _______  _______ 
	(  ____ \(  ___  )(  ____ )(  ____ \\__   __/(  ___  )| \    /\(  ____ \(  ____ )
	| (    \/| (   ) || (    )|| (    \/   ) (   | (   ) ||  \  / /| (    \/| (    )|
	| |      | (___) || (____)|| (__       | |   | (___) ||  (_/ / | (__    | (____)|
	| |      |  ___  ||     __)|  __)      | |   |  ___  ||   _ (  |  __)   |     __)
	| |      | (   ) || (\ (   | (         | |   | (   ) ||  ( \ \ | (      | (\ (   
	| (____/\| )   ( || ) \ \__| (____/\   | |   | )   ( ||  /  \ \| (____/\| ) \ \__
	(_______/|/     \||/   \__/(_______/   )_(   |/     \||_/    \/(_______/|/   \__/
	`
)

var (
	legend = []string{
		"[light_red]Major[reset] – Incompatible changes",
		"[light_yellow]Minor[reset] – New functionality in a backwards-compatible manner",
		"[light_green]Patch[reset] – Backwards-compatible bug fixes",
	}
)

func formatList(in []string) string {
	columnConf := columnize.DefaultConfig()
	columnConf.Glue = " = "
	columnConf.Prefix = "\t"
	return columnize.Format(in, columnConf)
}

func printsError(err error) {
	msg := fmt.Sprintf("[light_red]Error[reset] %v", err)
	fmt.Println(colorstring.Color(msg))
}

func printsRepos(repos []*repository) {
	for _, repo := range repos {
		if !repo.hasUpdate {
			continue
		}
		version := repo.version.Raw
		if len(version) > 8 {
			version = version[:8]
		}
		basic := []string{
			fmt.Sprintf("Dependency|%s", repo.dep.Repository),
			fmt.Sprintf("Version|%s", version),
		}
		if repo.release != nil {
			basic = append(basic, fmt.Sprintf("Date|%s (%s)", repo.release.DateRaw, humanize.Time(repo.release.Date)))
		}
		basic = append(basic, fmt.Sprintf("Changelog|%v", repo.changelog != nil))
		if repo.yanked {
			basic = append(basic, "Yanked|[light_red]true[reset]")
		} else {
			basic = append(basic, "Yanked|false")
		}
		if len(repo.major) > 0 {
			basic = append(basic, fmt.Sprintf("Major Update|[light_red]%s[reset]", repo.major))
		}
		if len(repo.minor) > 0 {
			basic = append(basic, fmt.Sprintf("Minor Update|[light_yellow]%s[reset]", repo.minor))
		}
		if len(repo.patch) > 0 {
			basic = append(basic, fmt.Sprintf("Patch Update|[light_green]%s[reset]", repo.patch))
		}
		fmt.Println(colorstring.Color(formatList(basic)) + "\n")
	}
}

func printsErrors(repos []*repository) {
	for _, repo := range repos {
		if len(repo.errors) == 0 {
			continue
		}
		basic := []string{
			fmt.Sprintf("Dependency|%s", repo.dep.Repository),
		}
		for _, err := range repo.errors {
			basic = append(basic, fmt.Sprintf("Error|%v", err))
		}
		fmt.Println(formatList(basic) + "\n")
	}
}

func printsHeader() {
	fmt.Println(header)
	for _, line := range legend {
		fmt.Println(colorstring.Color("\t" + line))
	}
	fmt.Println()
}

func SanitizedVersion(ver string) string {
	prefs := []string{"ver.", "ver", "v.", "v"}
	for _, pref := range prefs {
		ver = strings.TrimPrefix(ver, pref)
	}
	return ver
}

func FindNewestMajorTag(versions []*Semver, version *Semver) string {
	var originalTags []string
	for _, tag := range versions {
		if version.Version.LessThan(*tag.Version) &&
			tag.Version.Major > version.Version.Major {
			originalTags = append(originalTags, tag.Raw)
		}
	}
	if originalTags == nil {
		return ""
	}
	sort.Strings(originalTags)
	return originalTags[len(originalTags)-1]
}

func FindNewestMinorTag(versions []*Semver, version *Semver) string {
	var originalTags []string
	for _, tag := range versions {
		if version.Version.LessThan(*tag.Version) &&
			tag.Version.Major == version.Version.Major &&
			tag.Version.Minor > version.Version.Minor {
			originalTags = append(originalTags, tag.Raw)
		}
	}
	if originalTags == nil {
		return ""
	}
	sort.Strings(originalTags)
	return originalTags[len(originalTags)-1]
}

func FindNewestPatchTag(versions []*Semver, version *Semver) string {
	var originalTags []string
	for _, tag := range versions {
		if version.Version.LessThan(*tag.Version) &&
			tag.Version.Major == version.Version.Major &&
			tag.Version.Minor == version.Version.Minor &&
			tag.Version.Patch > version.Version.Patch {
			originalTags = append(originalTags, tag.Raw)
		}
	}
	if originalTags == nil {
		return ""
	}
	sort.Strings(originalTags)
	return originalTags[len(originalTags)-1]
}

func MatchRegExpString(pattern, text string) bool {
	if len(pattern) == 0 {
		return true
	}
	re := regexp.MustCompile(pattern)
	return re.MatchString(text)
}
