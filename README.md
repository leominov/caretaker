```shell
 _______  _______  _______  _______ _________ _______  _        _______  _______ 
(  ____ \(  ___  )(  ____ )(  ____ \\__   __/(  ___  )| \    /\(  ____ \(  ____ )
| (    \/| (   ) || (    )|| (    \/   ) (   | (   ) ||  \  / /| (    \/| (    )|
| |      | (___) || (____)|| (__       | |   | (___) ||  (_/ / | (__    | (____)|
| |      |  ___  ||     __)|  __)      | |   |  ___  ||   _ (  |  __)   |     __)
| |      | (   ) || (\ (   | (         | |   | (   ) ||  ( \ \ | (      | (\ (   
| (____/\| )   ( || ) \ \__| (____/\   | |   | )   ( ||  /  \ \| (____/\| ) \ \__
(_______/|/     \||/   \__/(_______/   )_(   |/     \||_/    \/(_______/|/   \__/
```

[![Build Status](https://travis-ci.com/leominov/caretaker.svg?token=tyxzVzn67Z9UV2wuxhSV&branch=master)](https://travis-ci.com/leominov/caretaker)

## Supported Sources

### Ansible requirements file

File should be named as `requirements.yml` or `requirements.yaml`. Example input:

```yaml
---
- src: geerlingguy.java
  version: 1.9.0
- src: bertvv.samba
  version: 2.5.0
```

Example output:

```
Here is some updates for you, enjoy:

	Dependency   = https://galaxy.ansible.com/geerlingguy/java
	Version      = 1.9.0
	Changelog    = false
	Yanked       = false
	Minor Update = 1.10.0
	Patch Update = 1.9.7

	Dependency   = https://galaxy.ansible.com/bertvv/samba
	Version      = 2.5.0
	Date         = 2017-11-21 (2 years ago)
	Changelog    = true
	Yanked       = false
	Minor Update = 2.7.1
	Patch Update = 2.5.1
```

### Environment variables

* `CARETAKER_SRC` – link on a repository;
* `CARETAKER_VERSION` – current version;
* `CARETAKER_PROVIDER` – provider's name.

## Supported Providers

* Ansible Galaxy
* Atlassian Bitbucket
* Atlassian Stash (`STASH_TOKEN`)
* Docker Registry
* GitHub
* GitLab (`GITLAB_TOKEN`)

## Configuration file

Configuration stored in `.caretaker.yml` file. Here is an example:

```yaml
---
# yamllint disable rule:truthy
providers:
  - type: galaxy
    hostname: galaxy.ansible.com
  - type: github
    hostname: github.com
  - type: stash
    hostname: stash.your-company.org
  - type: gitlab
    hostname: gitlab.your-company.org
fail:
  on:
    patch: true
    minor: true
    major: true
exclude:
  - group/(project1|project2)
  - repository: group/(project1|project2)
  - version: 1.0.0
  - provider: galaxy
  - hostname: stash.your-company.org
```

## Command-line flags

```
-insecure
	Allow connections to SSL providers without certs
-version
	Prints version and exit
```
