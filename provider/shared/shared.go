package shared

import (
	"crypto/tls"
	"net/http"
	"time"
)

var (
	NetTransport = &http.Transport{
		TLSHandshakeTimeout: 5 * time.Second,
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: false,
		},
	}
	HttpClient = &http.Client{
		Timeout:   time.Second * 10,
		Transport: NetTransport,
	}
)
