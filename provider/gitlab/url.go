package gitlab

import (
	"errors"
	"net/url"
	"strings"
)

type URL struct {
	Scheme      string
	Host        string
	PathEscaped string
	Path        string
}

func Parse(link string) (*URL, error) {
	if len(link) == 0 {
		return nil, errors.New("empty link")
	}
	u, err := url.Parse(link)
	if err != nil {
		return nil, err
	}
	path := strings.TrimSuffix(u.Path, ".git")
	path = strings.Trim(path, "/")
	gurl := &URL{
		Scheme:      u.Scheme,
		Host:        u.Hostname(),
		PathEscaped: url.PathEscape(path),
		Path:        path,
	}
	return gurl, nil
}
