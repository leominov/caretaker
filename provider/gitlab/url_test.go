package gitlab

import "testing"

func TestParse(t *testing.T) {
	tests := []struct {
		link, host, pathEscaped, path string
	}{
		{
			link:        "git+ssh://git@gitlab-rnd.local/devops/ansible-roles/zookeeper-role.git",
			host:        "gitlab-rnd.local",
			pathEscaped: "devops%2Fansible-roles%2Fzookeeper-role",
			path:        "devops/ansible-roles/zookeeper-role",
		},
		{
			link:        "https://gitlab-rnd.local/devops/cloudsql-tools.git?arg=1",
			host:        "gitlab-rnd.local",
			pathEscaped: "devops%2Fcloudsql-tools",
			path:        "devops/cloudsql-tools",
		},
		{
			link:        "https://l.aminov@gitlab-rnd.local/devops-public/kustomize/rabbitmq.git",
			host:        "gitlab-rnd.local",
			pathEscaped: "devops-public%2Fkustomize%2Frabbitmq",
			path:        "devops-public/kustomize/rabbitmq",
		},
	}
	for _, test := range tests {
		u, err := Parse(test.link)
		if err != nil {
			t.Error(err)
		}
		if u.Host != test.host {
			t.Errorf("Must be %s, but got %s", test.host, u.Host)
		}
		if u.PathEscaped != test.pathEscaped {
			t.Errorf("Must be %s, but got %s", test.pathEscaped, u.PathEscaped)
		}
		if u.Path != test.path {
			t.Errorf("Must be %s, but got %s", test.path, u.Path)
		}
	}
	_, err := Parse("")
	if err == nil {
		t.Error("Must be error, but got nil")
	}
	_, err = Parse(":")
	if err == nil {
		t.Error("Must be error, but got nil")
	}
}
