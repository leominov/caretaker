package gitlab

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGitlab_GetChangelog(t *testing.T) {
	g := New()
	_, err := g.GetChangelog("https://gitlab.com/leominov", "master")
	assert.Error(t, err)
	_, err = g.GetChangelog("https://gitlab.com/leominov/public", "master")
	assert.Error(t, err)
	log, err := g.GetChangelog("https://gitlab.com/leominov/gitlab-ci-linter", "master")
	assert.NoError(t, err)
	assert.GreaterOrEqual(t, len(log), 1)
}

func TestGitlab_GetTags(t *testing.T) {
	g := New()
	_, err := g.GetTags("https://gitlab.com/leominov")
	assert.Error(t, err)
	tags, err := g.GetTags("https://gitlab.com/leominov/public")
	assert.NoError(t, err)
	assert.Equal(t, 0, len(tags))
	tags, err = g.GetTags("https://gitlab.com/leominov/gitlab-ci-linter")
	assert.NoError(t, err)
	assert.GreaterOrEqual(t, len(tags), 1)
}
