package gitlab

import (
	"fmt"
	"net/http"
	"os"

	"github.com/xanzy/go-gitlab"

	"github.com/leominov/caretaker/provider/shared"
)

type Gitlab struct {
	httpCli *http.Client
	token   string
}

func New() *Gitlab {
	return &Gitlab{
		httpCli: shared.HttpClient,
		token:   os.Getenv("GITLAB_TOKEN"),
	}
}

func (s *Gitlab) client(baseURL *URL) (*gitlab.Client, error) {
	u := fmt.Sprintf("%s://%s", baseURL.Scheme, baseURL.Host)
	return gitlab.NewClient(s.token,
		gitlab.WithHTTPClient(s.httpCli),
		gitlab.WithBaseURL(u),
	)
}

func (s *Gitlab) GetName() string {
	return "gitlab"
}

func (s *Gitlab) GetChangelog(link, ref string) (result []byte, err error) {
	gurl, err := Parse(link)
	if err != nil {
		return
	}
	cli, err := s.client(gurl)
	if err != nil {
		return
	}
	opt := &gitlab.GetRawFileOptions{
		Ref: &ref,
	}
	result, _, err = cli.RepositoryFiles.GetRawFile(gurl.Path, "CHANGELOG.md", opt)
	if err != nil {
		return
	}
	return
}

func (s *Gitlab) GetTags(link string) (result []string, err error) {
	gurl, err := Parse(link)
	if err != nil {
		return
	}
	cli, err := s.client(gurl)
	if err != nil {
		return
	}
	tags, _, err := cli.Tags.ListTags(gurl.Path, nil)
	if err != nil {
		return
	}
	for _, tag := range tags {
		result = append(result, tag.Name)
	}
	return
}
