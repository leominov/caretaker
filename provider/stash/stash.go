package stash

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/leominov/caretaker/provider/shared"
)

// https://docs.atlassian.com/DAC/rest/stash/3.11.6/stash-rest.html
type Stash struct {
}

func New() *Stash {
	return &Stash{}
}

func (s *Stash) GetName() string {
	return "stash"
}

func (s *Stash) GetChangelog(link, ref string) (result []byte, err error) {
	surl, err := Parse(link)
	if err != nil {
		return
	}
	changelogLink := fmt.Sprintf(
		"https://%s/projects/%s/repos/%s/raw/CHANGELOG.md?at=%s&raw",
		surl.Host,
		surl.Project,
		surl.Repository,
		ref,
	)
	req, err := http.NewRequest(http.MethodGet, changelogLink, nil)
	if err != nil {
		return
	}
	s.setAuth(req)
	resp, err := shared.HttpClient.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("unexpected status code: %s", resp.Status)
		return
	}
	result, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	if bytes.Contains(result, []byte(`<!DOCTYPE html>`)) {
		result = []byte{}
		err = errors.New("unexpected content type")
		return
	}
	return
}

func (s *Stash) GetTags(link string) (result []string, err error) {
	surl, err := Parse(link)
	if err != nil {
		return
	}
	tagsLink := fmt.Sprintf(
		"https://%s/rest/api/1.0/projects/%s/repos/%s/tags",
		surl.Host,
		surl.Project,
		surl.Repository,
	)
	tagsResp := &TagsResponse{}
	err = s.doRequest(http.MethodGet, tagsLink, nil, &tagsResp)
	if err != nil {
		return
	}
	for _, tag := range tagsResp.Tags {
		result = append(result, tag.DisplayID)
	}
	return
}

func (s *Stash) setAuth(r *http.Request) {
	token := os.Getenv("STASH_TOKEN")
	if len(token) == 0 {
		return
	}
	r.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
}

func (s *Stash) doRequest(method, path string, body interface{}, respObj interface{}) error {
	var buf io.Reader
	if body != nil {
		b, err := json.Marshal(body)
		if err != nil {
			return err
		}
		buf = bytes.NewBuffer(b)
	}
	req, err := http.NewRequest(method, path, buf)
	if err != nil {
		return err
	}
	s.setAuth(req)
	req.Header.Add("Content-Type", "application/json")
	req.Close = true
	resp, err := shared.HttpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusUnauthorized {
		return fmt.Errorf("Failed to read the repository: %v", resp.Status)
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Unexpected status code: %s", resp.Status)
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	return json.Unmarshal(b, &respObj)
}
