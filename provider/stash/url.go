package stash

import (
	"errors"
	"net/url"
	"path"
	"strings"
)

type URL struct {
	Host       string
	Project    string
	Repository string
}

func Parse(link string) (*URL, error) {
	if len(link) == 0 {
		return nil, errors.New("empty link")
	}
	u, err := url.Parse(link)
	if err != nil {
		return nil, err
	}
	proj, repo := path.Split(u.Path)
	repo = strings.TrimSuffix(repo, ".git")
	proj = path.Base(proj)
	surl := &URL{
		Host:       u.Hostname(),
		Project:    proj,
		Repository: repo,
	}
	return surl, nil
}
