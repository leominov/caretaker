package stash

import (
	"testing"
)

func TestParse(t *testing.T) {
	tests := []struct {
		link, host, proj, repo string
	}{
		{
			link: "https://stash.local/scm/ans_int/prometheus-mysqld-exporter.git",
			host: "stash.local",
			proj: "ans_int",
			repo: "prometheus-mysqld-exporter",
		},
		{
			link: "https://l.aminov@stash.local/scm/do/vrrp_exporter.git",
			host: "stash.local",
			proj: "do",
			repo: "vrrp_exporter",
		},
		{
			link: "git+ssh://git@stash.local:7999/do_pub/ansible-postgresql-create-users.git",
			host: "stash.local",
			proj: "do_pub",
			repo: "ansible-postgresql-create-users",
		},
	}
	for _, test := range tests {
		surl, err := Parse(test.link)
		if err != nil {
			t.Error(err)
		}
		if surl.Host != test.host {
			t.Errorf("Must be %s, but got %s", test.host, surl.Host)
		}
		if surl.Project != test.proj {
			t.Errorf("Must be %s, but got %s", test.proj, surl.Project)
		}
		if surl.Repository != test.repo {
			t.Errorf("Must be %s, but got %s", test.proj, surl.Repository)
		}
	}
	_, err := Parse("")
	if err == nil {
		t.Error("Must be error, but got nil")
	}
	_, err = Parse(":")
	if err == nil {
		t.Error("Must be error, but got nil")
	}
}
