package stash

type TagsResponse struct {
	Tags []*Tag `json:"values"`
}

type Tag struct {
	ID               string `json:"id"`
	DisplayID        string `json:"displayId"`
	Type             string `json:"type"`
	LatestCommit     string `json:"latestCommit"`
	LattestChangeset string `json:"latestChangeset"`
	Hash             string `json:"hash"`
}
