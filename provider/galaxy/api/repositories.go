package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/leominov/caretaker/provider/shared"
)

// https://galaxy.ansible.com/api/v1/repositories/?name__iexact=ROLENAME&provider_namespace__namespace__name__iexact=ROLEAUTHOR
func GetRepositoryByName(namespace, name string) (*Repository, error) {
	tagsLink := fmt.Sprintf(
		"%s/repositories/?name__iexact=%s&provider_namespace__namespace__name__iexact=%s",
		baseURL,
		name,
		namespace,
	)
	req, err := http.NewRequest(http.MethodGet, tagsLink, nil)
	if err != nil {
		return nil, err
	}
	resp, err := shared.HttpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status code: %s", resp.Status)
	}
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	repoResponse := &RepositoryResponse{}
	err = json.Unmarshal(b, repoResponse)
	if err != nil {
		return nil, err
	}
	if len(repoResponse.Results) == 0 {
		return nil, errors.New("repository not found")
	}
	repo := repoResponse.Results[0]
	return repo, nil
}
