package api

type RepositoryResponse struct {
	Pagination
	Results []*Repository `json:"results"`
}

type Pagination struct {
	Count int `json:"count"`
}

type Repository struct {
	ID            int           `json:"id"`
	SummaryFields SummaryFields `json:"summary_fields"`
	ExternalURL   string        `json:"external_url"`
}

type SummaryFields struct {
	Versions     []*Version   `json:"versions"`
	Provider     Provider     `json:"provider"`
	LatestImport LatestImport `json:"latest_import"`
}

type LatestImport struct {
	State string `json:"state"`
}

type Provider struct {
	Name string `json:"name"`
}

type Version struct {
	Version     string `json:"version"`
	DownloadURL string `json:"download_url"`
}
