package api

import "testing"

func TestGetRepositoryByName(t *testing.T) {
	_, err := GetRepositoryByName("foo", "bar")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	_, err = GetRepositoryByName("geerlingguy", "java")
	if err != nil {
		t.Errorf("Must be a nil, but got an error: %v", err)
	}
}
