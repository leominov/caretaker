package galaxy

import "testing"

func TestGetTags(t *testing.T) {
	g := New()
	_, err := g.GetTags("https://galaxy.ansible.com/foo")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	_, err = g.GetTags("https://galaxy.ansible.com/foo/bar")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	_, err = g.GetTags("https://galaxy.ansible.com/geerlingguy/java")
	if err != nil {
		t.Errorf("Must be a nil, but got an error: %v", err)
	}
}

func TestGetChangelog(t *testing.T) {
	g := New()
	_, err := g.GetChangelog("https://galaxy.ansible.com/foo", "master")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	_, err = g.GetChangelog("https://galaxy.ansible.com/foo/bar", "master")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	_, err = g.GetChangelog("https://galaxy.ansible.com/idealista/airflow-role", "93356f359d562a1c8660deeff2d7a7679f45bfb9")
	if err != nil {
		t.Errorf("Must be a nil, but got an error: %v", err)
	}
}
