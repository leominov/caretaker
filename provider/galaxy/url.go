package galaxy

import (
	"errors"
	"net/url"
	"strings"
)

type URL struct {
	Namespace string
	Name      string
}

func Parse(link string) (*URL, error) {
	if len(link) == 0 {
		return nil, errors.New("empty link")
	}
	u, err := url.Parse(link)
	if err != nil {
		return nil, err
	}
	path := strings.Trim(u.Path, "/")
	parts := strings.Split(path, "/")
	if len(parts) != 2 {
		return nil, errors.New("incorrect link")
	}
	return &URL{
		Namespace: parts[0],
		Name:      parts[1],
	}, nil
}
