package galaxy

import (
	"fmt"
	"strings"

	"github.com/leominov/caretaker/provider/galaxy/api"
	"github.com/leominov/caretaker/provider/github"
)

type Galaxy struct {
	gh *github.Github
}

func New() *Galaxy {
	gh := github.New()
	return &Galaxy{
		gh: gh,
	}
}

func (g *Galaxy) GetName() string {
	return "galaxy"
}

func (g *Galaxy) GetChangelog(link, ref string) (result []byte, err error) {
	aurl, err := Parse(link)
	if err != nil {
		return
	}
	repo, err := api.GetRepositoryByName(aurl.Namespace, aurl.Name)
	if err != nil {
		return
	}
	providerName := repo.SummaryFields.Provider.Name
	providerName = strings.ToLower(providerName)
	// https: //galaxy.ansible.com/api/v1/providers/active/
	if providerName != "github" {
		err = fmt.Errorf("%s is not supported yet", providerName)
		return
	}
	return g.gh.GetChangelog(repo.ExternalURL, ref)
}

func (g *Galaxy) GetTags(link string) (result []string, err error) {
	aurl, err := Parse(link)
	if err != nil {
		return
	}
	repo, err := api.GetRepositoryByName(aurl.Namespace, aurl.Name)
	if err != nil {
		return
	}
	versions := repo.SummaryFields.Versions
	for _, version := range versions {
		result = append(result, version.Version)
	}
	return
}
