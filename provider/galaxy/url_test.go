package galaxy

import "testing"

func TestParse(t *testing.T) {
	testsErr := []string{
		"",
		"@#$%^&*()",
		"https://galaxy.ansible.com/foobar",
		"https://galaxy.ansible.com/foo/bar/foo",
	}
	for _, u := range testsErr {
		if _, err := Parse(u); err == nil {
			t.Error("Must be error, but got nil")
		}
	}
	tests := map[string]URL{
		"https://galaxy.ansible.com/foo/bar": URL{
			Namespace: "foo",
			Name:      "bar",
		},
		"https://galaxy.ansible.com/foo/bar.foo": URL{
			Namespace: "foo",
			Name:      "bar.foo",
		},
	}
	for link, u := range tests {
		url, err := Parse(link)
		if err != nil {
			t.Fatal(err)
		}
		if u.Name != url.Name {
			t.Errorf("Must be %s, but got %s", u.Name, url.Name)
		}
		if u.Namespace != url.Namespace {
			t.Errorf("Must be %s, but got %s", u.Namespace, url.Namespace)
		}
	}
}
