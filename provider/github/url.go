package github

import (
	"errors"
	"net/url"
	"strings"
)

type URL struct {
	Owner string
	Repo  string
}

func Parse(link string) (*URL, error) {
	if len(link) == 0 {
		return nil, errors.New("empty link")
	}
	u, err := url.Parse(link)
	if err != nil {
		return nil, err
	}
	path := strings.TrimSuffix(u.Path, ".git")
	path = strings.Trim(path, "/")
	parts := strings.Split(path, "/")
	if len(parts) != 2 {
		return nil, errors.New("incorrect link")
	}
	return &URL{
		Owner: parts[0],
		Repo:  parts[1],
	}, nil
}
