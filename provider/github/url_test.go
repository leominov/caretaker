package github

import "testing"

func TestParse(t *testing.T) {
	testsErr := []string{
		"",
		"@#$%^&*()",
		"https://github.com/foobar",
		"https://github.com/foo/bar/foo",
	}
	for _, u := range testsErr {
		if _, err := Parse(u); err == nil {
			t.Error("Must be error, but got nil")
		}
	}
	tests := map[string]URL{
		"https://github.com/foo/bar": URL{
			Owner: "foo",
			Repo:  "bar",
		},
		"https://github.com/foo/bar.foo": URL{
			Owner: "foo",
			Repo:  "bar.foo",
		},
	}
	for link, u := range tests {
		url, err := Parse(link)
		if err != nil {
			t.Fatal(err)
		}
		if u.Repo != url.Repo {
			t.Errorf("Must be %s, but got %s", u.Repo, url.Repo)
		}
		if u.Owner != url.Owner {
			t.Errorf("Must be %s, but got %s", u.Owner, url.Owner)
		}
	}
}
