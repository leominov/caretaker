package github

import (
	"context"

	"github.com/google/go-github/github"

	"github.com/leominov/caretaker/provider/shared"
)

type Github struct {
	cli *github.Client
}

func New() *Github {
	// https://github.com/google/go-github#authentication
	client := github.NewClient(shared.HttpClient)
	return &Github{
		cli: client,
	}
}

func (s *Github) GetName() string {
	return "github"
}

func (s *Github) GetChangelog(link, ref string) (result []byte, err error) {
	gurl, err := Parse(link)
	if err != nil {
		return
	}
	opt := &github.RepositoryContentGetOptions{
		Ref: ref,
	}
	content, _, _, err := s.cli.Repositories.GetContents(
		context.Background(),
		gurl.Owner,
		gurl.Repo,
		"CHANGELOG.md",
		opt,
	)
	if err != nil {
		return
	}
	body, err := content.GetContent()
	if err != nil {
		return
	}
	result = []byte(body)
	return
}

func (s *Github) GetTags(link string) (result []string, err error) {
	gurl, err := Parse(link)
	if err != nil {
		return
	}
	tags, _, err := s.cli.Repositories.ListTags(
		context.Background(),
		gurl.Owner,
		gurl.Repo,
		nil,
	)
	if err != nil {
		return
	}
	for _, tag := range tags {
		result = append(result, tag.GetName())
	}
	return
}
