package github

import (
	"testing"
)

func TestGetTags(t *testing.T) {
	g := New()
	_, err := g.GetTags("https://github.com/google")
	if err == nil {
		t.Error("Must be an error, but got a nil")
	}
	_, err = g.GetTags("https://github.com/google/not-found")
	if err == nil {
		t.Error("Must be an error, but got a nil")
	}
	tags, err := g.GetTags("https://github.com/google/go-cloud.git")
	if err != nil {
		t.Errorf("Must be a nil, but got an error: %v", err)
	}
	if len(tags) == 0 {
		t.Error("Empty tags list")
	}
}

func TestGetChangelog(t *testing.T) {
	g := New()
	_, err := g.GetChangelog("https://github.com/google", "master")
	if err == nil {
		t.Error("Must be an error, but got a nil")
	}
	_, err = g.GetChangelog("https://github.com/google/not-found", "master")
	if err == nil {
		t.Error("Must be an error, but got a nil")
	}
	chlog, err := g.GetChangelog("https://github.com/olivierlacan/keep-a-changelog.git", "master")
	if err != nil {
		t.Errorf("Must be a nil, but got an error: %v", err)
	}
	if len(chlog) == 0 {
		t.Error("Empty changelog")
	}
}
