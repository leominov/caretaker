package provider

import (
	"sort"

	"github.com/leominov/caretaker/provider/bitbucket"
	"github.com/leominov/caretaker/provider/docker"
	"github.com/leominov/caretaker/provider/galaxy"
	"github.com/leominov/caretaker/provider/github"
	"github.com/leominov/caretaker/provider/gitlab"
	"github.com/leominov/caretaker/provider/stash"
)

var (
	stashProvider     = stash.New()
	gitlabProvider    = gitlab.New()
	galaxyProvider    = galaxy.New()
	githubProvider    = github.New()
	dockerProvider    = docker.New()
	bitbucketProvider = bitbucket.New()

	Providers = map[string]Provider{
		stashProvider.GetName():     stashProvider,
		gitlabProvider.GetName():    gitlabProvider,
		galaxyProvider.GetName():    galaxyProvider,
		githubProvider.GetName():    githubProvider,
		dockerProvider.GetName():    dockerProvider,
		bitbucketProvider.GetName(): bitbucketProvider,
	}
)

func GetProviders() (result []string) {
	for name := range Providers {
		result = append(result, name)
	}
	sort.Strings(result)
	return
}

type Provider interface {
	GetName() string
	GetTags(link string) ([]string, error)
	GetChangelog(link string, ref string) ([]byte, error)
}
