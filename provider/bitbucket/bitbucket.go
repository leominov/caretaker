package bitbucket

import (
	"context"

	"github.com/wbrefvem/go-bitbucket"
)

type Bitbucket struct {
	cli *bitbucket.APIClient
}

func New() *Bitbucket {
	config := bitbucket.NewConfiguration()
	cli := bitbucket.NewAPIClient(config)
	return &Bitbucket{
		cli: cli,
	}
}

func (b *Bitbucket) GetName() string {
	return "bitbucket"
}

func (b *Bitbucket) GetChangelog(link, ref string) (result []byte, err error) {
	return nil, nil
}

func (b *Bitbucket) GetTags(link string) (result []string, err error) {
	burl, err := Parse(link)
	if err != nil {
		return
	}
	tags, _, err := b.cli.RefsApi.RepositoriesUsernameRepoSlugRefsTagsGet(
		context.Background(),
		burl.Owner,
		burl.Repo,
		nil,
	)
	if err != nil {
		return
	}
	for _, tag := range tags.Values {
		result = append(result, tag.Name)
	}
	return
}
