package bitbucket

import (
	"errors"
	"fmt"
	"net/url"
	"path/filepath"
	"strings"
)

type URL struct {
	BaseURL string
	Owner   string
	Repo    string
}

func Parse(link string) (*URL, error) {
	if len(link) == 0 {
		return nil, errors.New("empty link")
	}
	u, err := url.Parse(link)
	if err != nil {
		return nil, err
	}
	path := strings.TrimSuffix(u.Path, ".git")
	path = strings.TrimPrefix(path, "/")
	owner := filepath.Dir(path)
	repo := filepath.Base(path)
	return &URL{
		BaseURL: fmt.Sprintf("%s://%s", u.Scheme, u.Host),
		Owner:   owner,
		Repo:    repo,
	}, nil
}
