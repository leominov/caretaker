package bitbucket

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParse(t *testing.T) {
	tests := []struct {
		in  string
		out *URL
	}{
		{
			in: "https://bitbucket.org/leoderoko/foobar",
			out: &URL{
				BaseURL: "https://bitbucket.org",
				Owner:   "leoderoko",
				Repo:    "foobar",
			},
		},
		{
			in:  ":",
			out: nil,
		},
	}
	for _, test := range tests {
		out, _ := Parse(test.in)
		assert.Equal(t, test.out, out)
	}
}
