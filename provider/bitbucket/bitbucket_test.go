package bitbucket

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBitbucket_GetTags(t *testing.T) {
	b := New()
	_, err := b.GetTags("https://bitbucket.org/leoderoko/foobar")
	assert.Error(t, err)
	tags, err := b.GetTags("https://bitbucket.org/leoderoko/test")
	assert.NoError(t, err)
	assert.GreaterOrEqual(t, len(tags), 1)
}
