package docker

import (
	"fmt"

	"github.com/google/go-containerregistry/pkg/name"
	"github.com/google/go-containerregistry/pkg/v1/remote"
)

type Docker struct{}

func New() *Docker {
	return &Docker{}
}

func (d *Docker) GetName() string {
	return "docker"
}

func (d *Docker) GetChangelog(_, _ string) (result []byte, err error) {
	return nil, nil
}

func (d *Docker) GetTags(link string) (result []string, err error) {
	ref, err := name.ParseReference(link)
	if err != nil {
		return nil, fmt.Errorf("parsing reference %q: %v", link, err)
	}
	return remote.List(ref.Context())
}
