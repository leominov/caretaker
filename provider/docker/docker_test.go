package docker

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDocker_GetTags(t *testing.T) {
	d := New()
	_, err := d.GetTags("leoderoko/foobar")
	assert.Error(t, err)
	tags, err := d.GetTags("alpine")
	assert.NoError(t, err)
	assert.GreaterOrEqual(t, len(tags), 1)
}
