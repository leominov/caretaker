// +build !internal

package main

var (
	defaultConfig = &Config{
		Providers: []*Provider{
			{
				Hostname: "github.com",
				Scheme:   "https",
				Type:     "github",
			},
			{
				Hostname: "galaxy.ansible.com",
				Scheme:   "https",
				Type:     "galaxy",
			},
			{
				Hostname: "gitlab.com",
				Scheme:   "https",
				Type:     "gitlab",
			},
			{
				Hostname: "bitbucket.com",
				Scheme:   "https",
				Type:     "bitbucket",
			},
		},
	}
)

func (c *Config) defineDefaults() {
	for _, prv := range c.Providers {
		if len(prv.Scheme) == 0 {
			prv.Scheme = "https"
		}
	}
	for _, defPrv := range defaultConfig.Providers {
		match := false
		for _, prv := range c.Providers {
			if prv.Hostname != defPrv.Hostname {
				continue
			}
			match = true
			break
		}
		if !match {
			c.Providers = append(c.Providers, defPrv)
		}
	}
}
