module github.com/leominov/caretaker

go 1.13

require (
	github.com/briandowns/spinner v1.8.0
	github.com/coreos/go-semver v0.3.0
	github.com/dustin/go-humanize v1.0.0
	github.com/google/go-containerregistry v0.1.0
	github.com/google/go-github v17.0.0+incompatible
	github.com/leominov/changelog v0.0.0-20190401045853-1973e9040322
	github.com/mitchellh/colorstring v0.0.0-20190213212951-d06e56a500db
	github.com/prometheus/common v0.7.0
	github.com/prometheus/promu v0.5.0 // indirect
	github.com/ryanuber/columnize v2.1.0+incompatible
	github.com/stretchr/testify v1.5.1
	github.com/wbrefvem/go-bitbucket v0.0.0-20190128183802-fc08fd046abb
	github.com/xanzy/go-gitlab v0.32.0
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37
	gopkg.in/yaml.v2 v2.3.0
)
