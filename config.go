package main

import (
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Providers []*Provider   `yaml:"providers"`
	Exclude   []*FilterItem `yaml:"exclude"`
	Fail      struct {
		On struct {
			Patch bool `yaml:"patch"`
			Minor bool `yaml:"minor"`
			Major bool `yaml:"major"`
		} `yaml:"on"`
	} `yaml:"fail"`
}

type Provider struct {
	Hostname string `yaml:"hostname"`
	Scheme   string `yaml:"scheme"`
	Type     string `yaml:"type"`
}

func LoadFromFile(filename string) (*Config, error) {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return defaultConfig, nil
	}
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	c := &Config{}
	if err := yaml.Unmarshal(b, &c); err != nil {
		return nil, err
	}
	c.defineDefaults()
	return c, nil
}
