package main

import (
	"fmt"
	"testing"

	"gopkg.in/yaml.v2"

	"github.com/stretchr/testify/assert"

	"github.com/leominov/caretaker/discoverer"
)

func TestFilterItem_MatchDependency(t *testing.T) {
	tests := []struct {
		dep    *discoverer.Dependency
		filter *FilterItem
		match  bool
	}{
		{
			dep: &discoverer.Dependency{
				Provider:   "stash",
				Source:     "ansible-requirements",
				Repository: "https://stash.local/scm/ans_int/rsyslog.git",
				Version:    "1.0.0",
			},
			filter: &FilterItem{
				Provider:   "stash",
				Repository: "",
				Version:    "",
			},
			match: true,
		},
		{
			dep: &discoverer.Dependency{
				Provider:   "stash",
				Source:     "ansible-requirements",
				Repository: "https://stash.local/scm/ans_int/rsyslog.git",
				Version:    "1.0.0",
			},
			filter: &FilterItem{
				Provider:   "stash",
				Repository: "https://stash.local/.*",
				Version:    "",
			},
			match: true,
		},
		{
			dep: &discoverer.Dependency{
				Provider:   "stash",
				Source:     "ansible-requirements",
				Repository: "https://stash.local/scm/ans_int/rsyslog.git",
				Version:    "1.0.0",
			},
			filter: &FilterItem{
				Provider:   "stash",
				Repository: "https://stash.local/scm/ans_int/rsyslog.git",
				Version:    "1.0.[0-1]",
			},
			match: true,
		},
		{
			dep: &discoverer.Dependency{
				Provider:   "stash",
				Source:     "ansible-requirements",
				Repository: "https://stash.local/scm/ans_int/rsyslog.git",
				Version:    "1.0.0",
			},
			filter: &FilterItem{
				Provider:   "github",
				Repository: "",
				Version:    "",
			},
			match: false,
		},
		{
			dep: &discoverer.Dependency{
				Provider:   "stash",
				Source:     "ansible-requirements",
				Repository: "https://stash.local/scm/ans_int/rsyslog.git",
				Version:    "1.0.0",
			},
			filter: &FilterItem{
				Provider:   "stash",
				Repository: ".*github.*",
				Version:    "",
			},
			match: false,
		},
		{
			dep: &discoverer.Dependency{
				Provider:   "stash",
				Source:     "ansible-requirements",
				Repository: "https://stash.local/scm/ans_int/rsyslog.git",
				Version:    "1.0.0",
			},
			filter: &FilterItem{
				Provider:   "stash",
				Repository: "https://stash.local/scm/ans_int/rsyslog.git",
				Version:    "2.0.0",
			},
			match: false,
		},
	}
	for i, test := range tests {
		t.Run(fmt.Sprintf("match %[1]d dep to %[1]d rule", i), func(t *testing.T) {
			assert.Equal(t, test.match, test.filter.MatchDependency(test.dep))
		})
	}
}

func TestFilterItem_UnmarshalYAML(t *testing.T) {
	tests := []struct {
		in  string
		out FilterItem
	}{
		{
			in: "foobar",
			out: FilterItem{
				Repository: "foobar",
			},
		},
		{
			in: "repository: foobar",
			out: FilterItem{
				Repository: "foobar",
			},
		},
		{
			in: "version: 1.0.0",
			out: FilterItem{
				Version: "1.0.0",
			},
		},
		{
			in: "provider: stash",
			out: FilterItem{
				Provider: "stash",
			},
		},
	}
	for _, test := range tests {
		var out FilterItem
		err := yaml.Unmarshal([]byte(test.in), &out)
		if !assert.NoError(t, err) {
			continue
		}
		assert.Equal(t, test.out, out)
	}
}
