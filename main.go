package main

import (
	"flag"
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/leominov/caretaker/discoverer"
	"github.com/leominov/caretaker/provider"
	"github.com/leominov/caretaker/provider/shared"
	"github.com/prometheus/common/version"
)

const (
	configurationFilename = ".caretaker.yml"
)

var (
	versionFlag  = flag.Bool("version", false, "Prints version and exit")
	insecureFlag = flag.Bool("insecure", false, "Allow connections to SSL providers without certs")
)

func realMain() int {
	flag.Parse()

	if *versionFlag {
		fmt.Println(version.Print("Caretaker"))
		fmt.Printf("Providers: %s\n", strings.Join(provider.GetProviders(), ", "))
		fmt.Printf("Discoverers: %s\n", strings.Join(discoverer.GetDiscoverers(), ", "))
		return 0
	}

	if *insecureFlag {
		shared.NetTransport.TLSClientConfig.InsecureSkipVerify = true
	}

	wd, err := os.Getwd()
	if err != nil {
		printsError(fmt.Errorf("failed to get working directory: %v", err))
		return 1
	}

	config, err := LoadFromFile(path.Join(wd, configurationFilename))
	if err != nil {
		printsError(fmt.Errorf("failed to load configuration: %v", err))
		return 1
	}

	caretaker := &Caretaker{
		WorkingDir: wd,
		Config:     config,
	}

	return caretaker.Run()
}

func main() {
	os.Exit(realMain())
}
