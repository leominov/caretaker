package environ

import (
	"os"
	"testing"
)

func flushEnv() error {
	if err := os.Unsetenv(sourceKey); err != nil {
		return err
	}
	if err := os.Unsetenv(versionKey); err != nil {
		return err
	}
	if err := os.Unsetenv(providerKey); err != nil {
		return err
	}
	return nil
}

func setEnv(repo, ver, prv string) error {
	if err := os.Setenv(sourceKey, repo); err != nil {
		return err
	}
	if err := os.Setenv(versionKey, ver); err != nil {
		return err
	}
	if err := os.Setenv(providerKey, prv); err != nil {
		return err
	}
	return nil
}

func TestDiscover(t *testing.T) {
	if err := flushEnv(); err != nil {
		t.Fatal(err)
	}
	items, err := Discover("wd")
	if err != nil {
		t.Error(err)
	}
	if len(items) > 0 {
		t.Errorf("Must be 0, but got %d", len(items))
	}
	if err := setEnv("https://repo.local", "1.0.0", "github"); err != nil {
		t.Fatal(err)
	}
	items, err = Discover("wd")
	if err != nil {
		t.Error(err)
	}
	if len(items) != 1 {
		t.Errorf("Must be 1, but got %d", len(items))
	}
	if err := setEnv("http://[fe80::%31]:8080/", "1.0.0", "github"); err != nil {
		t.Fatal(err)
	}
	_, err = Discover("wd")
	if err == nil {
		t.Error("Must be error, but got nil")
	}
}
