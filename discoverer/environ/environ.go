package environ

import (
	"net/url"
	"os"

	"github.com/leominov/caretaker/discoverer"
)

const (
	discovererName = "environ"

	sourceKey   = "CARETAKER_SRC"
	versionKey  = "CARETAKER_VERSION"
	providerKey = "CARETAKER_PROVIDER"
)

func init() {
	discoverer.RegisterDiscoverer(discovererName, Discover)
}

func Discover(_ string) ([]*discoverer.Dependency, error) {
	src := os.Getenv(sourceKey)
	ver := os.Getenv(versionKey)
	provider := os.Getenv(providerKey)
	if len(src) == 0 || len(ver) == 0 || len(provider) == 0 {
		return nil, nil
	}
	u, err := url.Parse(src)
	if err != nil {
		return nil, err
	}
	item := &discoverer.Dependency{
		Provider:   provider,
		Source:     "environ",
		Repository: src,
		Version:    ver,
		URL:        u,
	}
	return []*discoverer.Dependency{item}, nil
}
