package ansiblerequirements

import (
	"net/url"
	"strings"
)

func IsGalaxyRole(src string) bool {
	u, err := url.Parse(src)
	if err != nil {
		return false
	}
	if len(u.Host) > 0 {
		return false
	}
	// https://galaxy.ansible.com/docs/contributing/creating_role.html#role-names
	if !strings.Contains(src, ".") {
		return false
	}
	return true
}

func GalaxyLink(src string) string {
	result := strings.Replace(src, ".", "/", 1)
	result = galaxyURL + result
	return result
}
