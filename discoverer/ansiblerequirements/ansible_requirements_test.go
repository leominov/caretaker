package ansiblerequirements

import "testing"

func TestDiscover(t *testing.T) {
	_, err := Discover("test_data/discoverer/single")
	if err != nil {
		t.Error(err)
	}
	_, err = Discover("test_data/discoverer/galaxy")
	if err != nil {
		t.Error(err)
	}
	_, err = Discover("test_data/discoverer/invalid_parse")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	_, err = Discover("test_data/discoverer/invalid_src")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
}
