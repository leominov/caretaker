package ansiblerequirements

import "testing"

func TestIsGalaxyRole(t *testing.T) {
	tests := map[string]bool{
		"https://google.com": false,
		"foobar":             false,
		"foo.bar":            true,
		"foo.bar.foo":        true,
		"@#$%^&*()":          false,
	}
	for src, isRalaxyRole := range tests {
		ok := IsGalaxyRole(src)
		if ok != isRalaxyRole {
			t.Errorf("Must be %v, but got %v for %s", isRalaxyRole, ok, src)
		}
	}
}

func TestGalaxyLink(t *testing.T) {
	tests := map[string]string{
		"foobar":      galaxyURL + "foobar",
		"foo.bar":     galaxyURL + "foo/bar",
		"foo.bar.foo": galaxyURL + "foo/bar.foo",
	}
	for in, out := range tests {
		link := GalaxyLink(in)
		if out != link {
			t.Errorf("Must be %s, but got %s", out, link)
		}
	}
}
