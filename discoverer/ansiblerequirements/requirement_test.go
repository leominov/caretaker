package ansiblerequirements

import "testing"

func TestParse(t *testing.T) {
	reqs, err := Parse("test_data/parser/requirements_single.yml")
	if err != nil {
		t.Error(err)
	}
	if len(reqs) != 1 {
		t.Errorf("Must be 1, but got %d", len(reqs))
	}
	reqs, err = Parse("test_data/parser/requirements_include_valid.yml")
	if err != nil {
		t.Error(err)
	}
	if len(reqs) != 1 {
		t.Errorf("Must be 1, but got %d", len(reqs))
	}
	_, err = Parse("test_data/parser/requirements_include_invalid.yml")
	if err == nil {
		t.Error("Must be error, but got nil")
	}
	_, err = Parse("test_data/parser/not_found.yml")
	if err == nil {
		t.Error("Must be error, but got nil")
	}
	_, err = Parse("test_data/parser/requirements_json.yml")
	if err == nil {
		t.Error("Must be error, but got nil")
	}
}
