package ansiblerequirements

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Requirement struct {
	SRC     string `yaml:"src"`
	Version string `yaml:"version"`
	SCM     string `yaml:"scm"`
	Name    string `yaml:"name"`
	Include string `yaml:"include,omitempty"`
}

func Parse(filename string) ([]*Requirement, error) {
	var (
		requirementsRaw []*Requirement
		requirements    []*Requirement
	)
	out, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(out, &requirementsRaw)
	if err != nil {
		return nil, fmt.Errorf("failed to parse %s: %v", filename, err)
	}
	for _, req := range requirementsRaw {
		if len(req.Include) != 0 {
			requirementsIncluded, err := Parse(req.Include)
			if err != nil {
				return nil, err
			}
			requirements = append(requirements, requirementsIncluded...)
			continue
		}
		requirements = append(requirements, req)
	}
	return requirements, nil
}
