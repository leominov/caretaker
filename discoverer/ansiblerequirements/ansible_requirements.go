package ansiblerequirements

import (
	"net/url"
	"os"
	"path"

	"github.com/leominov/caretaker/discoverer"
)

const (
	galaxyURL      = "https://galaxy.ansible.com/"
	discovererName = "ansible-requirements"
)

var (
	requirementsFiles = []string{
		"requirements.yaml",
		"requirements.yml",
	}
)

func init() {
	discoverer.RegisterDiscoverer(discovererName, Discover)
}

func requirementsToDiscovererDependencies(file string, reqs []*Requirement) ([]*discoverer.Dependency, error) {
	var result []*discoverer.Dependency
	for _, req := range reqs {
		provider := ""
		if IsGalaxyRole(req.SRC) {
			req.SRC = GalaxyLink(req.SRC)
			provider = "galaxy"
		}
		u, err := url.Parse(req.SRC)
		if err != nil {
			return nil, err
		}
		result = append(result, &discoverer.Dependency{
			Provider:   provider,
			Source:     file,
			Repository: req.SRC,
			Version:    req.Version,
			URL:        u,
		})
	}
	return result, nil
}

func Discover(wd string) ([]*discoverer.Dependency, error) {
	var result []*discoverer.Dependency
	for _, reqFile := range requirementsFiles {
		reqFilePath := path.Join(wd, reqFile)
		if _, err := os.Stat(reqFilePath); os.IsNotExist(err) {
			continue
		}
		requirements, err := Parse(reqFilePath)
		if err != nil {
			return nil, err
		}
		items, err := requirementsToDiscovererDependencies(reqFilePath, requirements)
		if err != nil {
			return nil, err
		}
		result = append(result, items...)
	}
	return result, nil
}
