package discoverer

import (
	"fmt"
	"net/url"
	"sort"
)

var (
	discoverers = map[string]Discoverer{}
)

type Dependency struct {
	Provider   string
	Source     string
	Repository string
	Version    string
	URL        *url.URL
}

type Discoverer func(wd string) ([]*Dependency, error)

func RegisterDiscoverer(name string, fn Discoverer) {
	discoverers[name] = fn
}

func GetDiscoverers() (result []string) {
	for name := range discoverers {
		result = append(result, name)
	}
	sort.Strings(result)
	return
}

func Discover(wd string) ([]*Dependency, error) {
	var result []*Dependency
	for name, discoverer := range discoverers {
		dependencies, err := discoverer(wd)
		if err != nil {
			return nil, fmt.Errorf("failed to discover dependencies from %s: %v", name, err)
		}
		if len(dependencies) == 0 {
			continue
		}
		result = append(result, dependencies...)
	}
	return result, nil
}
