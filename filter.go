package main

import (
	"github.com/leominov/caretaker/discoverer"
)

type FilterItem struct {
	Provider   string `yaml:"provider"`
	Repository string `yaml:"repository"`
	Version    string `yaml:"version"`
	Hostname   string `yaml:"hostname"`
}

type filterItemType FilterItem

func (f *FilterItem) UnmarshalYAML(unmarshal func(interface{}) error) error {
	if err := unmarshal((*filterItemType)(f)); err == nil {
		return nil
	}
	var str string
	err := unmarshal(&str)
	if err != nil {
		return err
	}
	f.Repository = str
	return nil
}

func (f *FilterItem) MatchDependency(dependency *discoverer.Dependency) bool {
	if !MatchRegExpString(f.Provider, dependency.Provider) {
		return false
	}
	if !MatchRegExpString(f.Repository, dependency.Repository) {
		return false
	}
	if !MatchRegExpString(f.Version, dependency.Version) {
		return false
	}
	if dependency.URL != nil && !MatchRegExpString(f.Hostname, dependency.URL.Host) {
		return false
	}
	return true
}
