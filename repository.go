package main

import (
	"errors"

	"github.com/coreos/go-semver/semver"
	"github.com/leominov/caretaker/discoverer"
	"github.com/leominov/changelog"
)

type Semver struct {
	Version *semver.Version
	Raw     string
}

type repository struct {
	version   *Semver
	dep       *discoverer.Dependency
	changelog *changelog.Changelog
	release   *changelog.Release
	errors    []error
	tags      []*Semver
	minor     string
	major     string
	patch     string
	hasUpdate bool
	yanked    bool
}

func (r *repository) addError(text string) {
	r.errors = append(r.errors, errors.New(text))
}
